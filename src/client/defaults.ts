import { ClientSettings } from "./types";

export const DefaultClientSettings: ClientSettings = {
  loginMode: "terminate",
  logTimestamps: true,
};
