import { AuthRecord, ClassicAccount } from "../shared/ichibotrpc_types";
import { IchibotSingle, IchibotSingleEmitter } from "./singleclient";

// all setting values should be lower case
export const ClientSettingsLoginModes = ["terminate", "stay-running"] as const;

export type ClientSettingsLoginMode = (typeof ClientSettingsLoginModes)[number];

export const ClientSettingsBooleans = ["true", "false"] as const;

export type ClientSettingsBoolean = (typeof ClientSettingsBooleans)[number];

export const ClientSettingNames = ["loginMode", "logTimestamps"] as const;

export type ClientSettingName = (typeof ClientSettingNames)[number];

/* eslint-disable-next-line @typescript-eslint/consistent-type-definitions */
export type ClientSettings = {
  loginMode: ClientSettingsLoginMode;
  logTimestamps: boolean;
};

export const NETWORK_STATUS = {
  connecting: "connecting",
  connected: "connected",
  reconnecting: "reconnecting",
  closing: "closing",
  closed: "closed",
} as const;

export interface ConnectionStatus {
  network: (typeof NETWORK_STATUS)[keyof typeof NETWORK_STATUS];
  lastKnownInstanceConnected?: number; // timestamp
  lastKnownKeepAlive?: number;
}

export interface Connection {
  account: ClassicAccount;
  auth: AuthRecord;
  conn: {
    emitter: IchibotSingleEmitter;
    single: IchibotSingle;
  };
  status: ConnectionStatus;
  firstSuccess: boolean;
}
