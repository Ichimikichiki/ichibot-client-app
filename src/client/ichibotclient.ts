import { Logger } from "../shared/util";
import { ExchangeLabel } from "../shared/types";

export interface ClientDB {
  push: (key: string, value: string | Record<string, unknown>) => void;
  delete: (key: string) => void;
  filter: <T>(
    root: string,
    fn: (item: T, key: string | number) => boolean,
  ) => T[] | undefined;
}

export interface IchibotClientOpts {
  wsUrlA: string;
  omitConnectionQueryString?: boolean;
  getDataSafe: <T>(path: string, defaultValue: T) => T;
  logger: Logger;
  readInitFile: (exchange: ExchangeLabel) => { initLines: string[] };
  saveCmdToInit: (
    exchange: ExchangeLabel,
    contextSymbol: string,
    lineMatchers: (string | null)[],
    cmd: string | null,
    reverse?: boolean,
    multiple?: boolean,
  ) => void;
  debug?: boolean;
  clientDB: ClientDB;
  process: {
    exit: ((ret: number) => void) | ((ret: number) => never);
  };
  io: {
    query?: (q: string) => Promise<string>;
    setPrompt: (p: string, preserveCursor?: boolean) => void;
  };
  startWithFriendlyName?: string;
  binaryDecoder?: (payload: Uint8Array) => Promise<unknown[]>;
}
