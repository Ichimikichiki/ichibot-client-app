import { getInitInstrument, Logger } from "./shared/util";
import { JsonDB } from "node-json-db";
import { Config as JsonDBConfig } from "node-json-db/dist/lib/JsonDBConfig";
import { join as pathJoin } from "path";
import { writeFileSync, existsSync, readFileSync } from "fs";
import { flatten } from "ramda";
import { CONSOLE_COLORS, SETTINGS_KEY } from "./client/constants";
import { ExchangeLabel } from "./shared/types";
import MasterClient from "./client/masterclient";
import ansiEscapes from "ansi-escapes";
import { outputPrinter, r1 } from "./printer";
import { decodeMultiple } from "cbor-x";

const warnOncePerExchange = new Set<ExchangeLabel>();

const COL = CONSOLE_COLORS;

const clientDB = new JsonDB(new JsonDBConfig("ichibot-config-db", true, true));

const getDataSafe = <T>(path: string, defaultValue: T): T => {
  if (clientDB.exists(path)) {
    return clientDB.getData(path);
  } else {
    return defaultValue;
  }
};

const LOG_TIMESTAMPS = getDataSafe<boolean>(
  `/${SETTINGS_KEY}/logTimestamps`,
  true,
);

const getInitFilePath = (exchange: ExchangeLabel): string =>
  pathJoin(process.cwd(), `initrun.${exchange}.txt`);

function readInitFile(exchange: ExchangeLabel): { initLines: string[] } {
  const filename = getInitFilePath(exchange);
  if (!existsSync(filename)) {
    if (!warnOncePerExchange.has(exchange)) {
      output.log(
        `First time running ichibot. Initrun file ${filename} will be made once you save your first alias.`,
      );
      warnOncePerExchange.add(exchange);
    }
    return { initLines: [] };
  }

  // Get lines & drop some extra ones. It doesn't matter if comments and empty lines are in but server doesn't need them either.
  const initLines = readFileSync(filename)
    .toString()
    .split("\n")
    .map((l) => l.trim())
    .filter((l) => l.length > 0)
    .filter((l) => !l.startsWith("#"));

  return {
    initLines,
  };
}

// If cmd is null this is a removal
function saveCmdToInit(
  exchange: ExchangeLabel,
  contextSymbol: string,
  lineMatchers: (string | null)[],
  cmd: string | null,
  reverse = false,
  multiple = false,
): void {
  const filename = getInitFilePath(exchange);

  const symFriendlyName = contextSymbol === "*" ? "global" : contextSymbol;
  if (cmd) {
    output.log(
      `Writing command "${cmd}" to ${symFriendlyName} initialization steps.`,
    );
  } else {
    output.log(
      /* eslint-disable-next-line @typescript-eslint/restrict-template-expressions */
      `Removing command "${lineMatchers[0]} ${lineMatchers[1]}" from ${symFriendlyName} initialization steps.`,
    );
  }

  const cmdWords = cmd
    ?.split(/\s+/)
    .map((a) => a.trim())
    .filter((a) => a !== "");

  const initLines = existsSync(filename)
    ? readFileSync(filename)
        .toString()
        .split("\n")
        .map((l) => l.trim())
    : [];

  interface LinesPerSym {
    sym: string;
    lines: string[];
  }
  const linesPerSym: LinesPerSym[] = [];
  let currentInstrument = getInitInstrument(exchange);

  let currentSymBlock: { sym: string; lines: string[] } = {
    sym: currentInstrument,
    lines: [],
  };

  for (const l of initLines) {
    const args = l.split(/\s+/).map((a) => a.trim());

    if (
      args[0] === "instrument" &&
      args.length > 1 &&
      args[1].toUpperCase() !== currentInstrument
    ) {
      linesPerSym.push(currentSymBlock);
      currentSymBlock = { sym: args[1].toUpperCase(), lines: [] };
      currentInstrument = currentSymBlock.sym;
    }

    currentSymBlock.lines.push(l);
  }

  linesPerSym.push(currentSymBlock);

  const matchingSymBlocks = (function findMatchingSymBlocks() {
    const isSymMatch = (b: LinesPerSym) => b.sym === contextSymbol;
    if (multiple) {
      return linesPerSym.filter(isSymMatch);
    }
    if (reverse) {
      const matches = linesPerSym.filter(isSymMatch);
      return matches.splice(-1);
    }
    const firstMatch = linesPerSym.find(isSymMatch);
    if (!firstMatch) return [];
    return [firstMatch];
  })();

  if (matchingSymBlocks.length === 0) {
    const symBlock = {
      sym: contextSymbol,
      lines: [
        `instrument ${contextSymbol}`,
        `### DEFINE ${symFriendlyName} aliases here`,
      ],
    };
    linesPerSym.push(symBlock);
    matchingSymBlocks.push(symBlock);
  }

  const isMatch = (l: string) => {
    if (lineMatchers.length === 0) {
      return false;
    }
    const args = l
      .split(/\s+/)
      .map((a) => a.trim())
      .filter((a) => a !== "");

    if (args.length < lineMatchers.length) {
      return false;
    }

    for (let i = 0; i < lineMatchers.length; ++i) {
      const lineMatch = lineMatchers[i];
      if (
        lineMatch !== null &&
        lineMatch.toLowerCase() !== args[i].toLowerCase().replace(":", "")
      ) {
        return false;
      }
    }

    return true;
  };

  const removeMarker = "#TOBEREMOVED#";

  matchingSymBlocks.forEach((symBlock) => {
    if (multiple) {
      for (const [k, v] of symBlock.lines.entries()) {
        if (isMatch(v)) {
          symBlock.lines[k] = cmdWords?.join(" ") ?? removeMarker;
        }
      }
      return;
    }
    if (reverse) symBlock.lines.reverse();

    const matchingLineIdx = symBlock.lines.findIndex(isMatch);

    if (matchingLineIdx > -1) {
      symBlock.lines[matchingLineIdx] = cmdWords?.join(" ") ?? removeMarker;
      if (reverse) symBlock.lines.reverse();
    } else if (cmdWords) {
      if (reverse) symBlock.lines.reverse();
      symBlock.lines.push(cmdWords.join(" "));
    } else {
      if (reverse) symBlock.lines.reverse();
    }
  });

  const newInitLines = flatten(linesPerSym.map((b) => b.lines)).filter(
    (l) => l !== removeMarker,
  );

  writeFileSync(filename, newInitLines.join("\n"));
}

// process.env.SERVER_URL = 'ws://ichibot.trade.local:8888'
/* eslint-disable @typescript-eslint/prefer-nullish-coalescing */
const wsUrlA =
  process.env.SERVER_URL ||
  process.env.SERVER_URL_A ||
  "wss://beta.ichibot.trade:2053";
/* eslint-enable @typescript-eslint/prefer-nullish-coalescing */

export interface Config {
  exchange: ExchangeLabel;
  apiKey: string;
  apiSecret: string;
  subAccount?: string;
  rateLimit?: [number, number];
}

async function query(msg: string): Promise<string> {
  const result: string = await new Promise((resolve) => {
    r1.question(msg, (answer) => {
      const cols = process.stdout.columns;
      if (!cols) {
        process.stdout.write(ansiEscapes.eraseLines(1));
        resolve(answer);
        return;
      }
      const len = r1.getPrompt().length + answer.length;
      const rows = Math.floor(len / cols);
      process.stdout.write(ansiEscapes.eraseLines(rows + 1));
      resolve(answer);
    });
  });
  return result;
}

const output: Logger = {
  log: (...args: unknown[]): void => {
    const s = [
      ...(LOG_TIMESTAMPS ? [new Date().toLocaleTimeString()] : []),
      args
        .map((x) => (typeof x === "string" ? x : JSON.stringify(x)))
        .join(" "),
      COL.Reset,
    ].join(" ");
    outputPrinter.write(s);
  },
  dir: (...args: Record<string, unknown>[]): void => {
    for (const arg of args) {
      for (const [k, v] of Object.entries(arg)) {
        outputPrinter.write(
          /* eslint-disable-next-line @typescript-eslint/restrict-template-expressions */
          [COL.Dim, `${k}:`, COL.Bright, `${v}`, COL.Reset].join(" "),
        );
      }
    }
    outputPrinter.write(COL.Reset);
  },
  warn: (...args: unknown[]): void => {
    output.log(COL.FgYellow, "WARN:", ...args);
  },
  error: (...args: unknown[]): void => {
    output.log(COL.FgRed, "ERROR:", ...args);
  },
  debug: (...args: unknown[]): void => {
    if (DEBUG) {
      output.log(
        `${COL.Dim}${COL.FgYellow}DEBUG:${COL.Reset}${COL.Dim}`,
        ...args,
      );
    }
  },
};

const DEBUG = !!process.env.DEBUG;
const myArgs = process.argv.slice(2);

function setPrompt(p: string, preserveCursor?: boolean) {
  r1.setPrompt(p);
  r1.prompt(preserveCursor);
}

async function go() {
  const mc = new MasterClient({
    wsUrlA,
    getDataSafe,
    logger: output,
    omitConnectionQueryString: true,
    debug: DEBUG,
    readInitFile,
    saveCmdToInit,
    clientDB,
    process: {
      exit: (ret) => process.exit(ret),
    },
    io: {
      setPrompt,
      query,
    },
    startWithFriendlyName: myArgs[0],
    binaryDecoder: (payload) => {
      try {
        const msg = decodeMultiple(payload);
        if (!msg) {
          return Promise.resolve([]);
        }
        return Promise.resolve(
          (msg as ({ jsonrpc: string } | null)[]).filter(
            (msg) => msg && typeof msg === "object" && msg.jsonrpc === "2.0",
          ),
        );
      } catch {
        // No need to send any decoding errors back to server
        return Promise.resolve([]);
      }
    },
  });
  await mc.start();
}

go().catch((e: unknown) => {
  console.error(e);
});
