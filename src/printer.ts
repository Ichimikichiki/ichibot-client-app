import ansiEscapes from "ansi-escapes";
import readline from "readline";

process.stdin.removeAllListeners("data");

const getCommandCompletions: readline.Completer = (s) => {
  return [[], s];
};

export const r1 = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  completer: (s: string) => getCommandCompletions(s),
  prompt: "command:",
});

class OutputPrinter {
  write(line: string) {
    const { rows } = r1.getCursorPos();
    const reset = ansiEscapes.cursorTo(0) + ansiEscapes.eraseLines(rows + 1);
    const output = reset + line + "\n".repeat(rows + 1);
    process.stdout.write(output);
    r1.prompt(true);
  }
}

export const outputPrinter = new OutputPrinter();
